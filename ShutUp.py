import tkinter as TK
import tkinter.filedialog as TKF

import os as OS
# os.system('python3 callspikes_RRauto.py')

colors = { \
              'bg': '#202020' \
            , 'fg': '#FFFFFF' \
            , 'hl': '#303030' \
            }

#______________________________________________________________________
### ToolBar ###
#______________________________________________________________________

class ToolBar(TK.Frame):
# class ToolBar(object):
    def __init__(self, buttons, orient_horz = True, **args):
        super(ToolBar, self).__init__(**args)
        # self.frame = TK.Frame(**args)
        self.buttons = {}
        if orient_horz:
            self.button_alignment = TK.RIGHT
            self.button_fill = TK.Y
        else:
            self.button_alignment = TK.BOTTOM
            self.button_fill = TK.X

        for nr, button in buttons:
            self.buttons[nr] = TK.Button( \
                                          master = self \
                                        , text = nr \
                                        , command = button \
                                        , bg = colors['bg'] \
                                        , fg = colors['fg'] \
                                        , highlightbackground = colors['hl'] \
                                        , highlightcolor = colors['fg'] \
                                        , borderwidth = 0 \
                                        , height = 4 \
                                        # , width = 4 \
                                        )
            self.buttons[nr].pack(side = self.button_alignment \
                                    , fill = self.button_fill)




#______________________________________________________________________
### Main Menu ###
#______________________________________________________________________

class ShutdownMenu(TK.Tk):
    def __init__(self, label = 'really shut down?', header = 'Shut Down'):
        super(ShutdownMenu, self).__init__()
        self.wm_title(header)
        TK.Label( \
                  master = self \
                , text = label \
                , bg = colors['bg'] \
                , fg = colors['fg'] \
                ).pack(side = 'top')

        self.configure(bg = colors['bg'])

        self.shut_options = ToolBar( \
                                buttons = [ \
                                              ['cancel', self.Quit] \
                                            , ['shutdown', self.ShutDown] \
                                            , ['reboot', self.Reboot] \
                                            #, ['Test', self.Test] \
                                          ] \
                                , orient_horz = True\
                                , master = self \
                                , relief = TK.FLAT \
                                , borderwidth = 0 \
                                , bg = colors['bg'] \
                                )
        self.shut_options.pack(side = 'top')


        self.bind('<Escape>', self.Quit)
        self.bind('<Return>', self.ShutDown)
        self.bind('<r>', self.Reboot)

        self.mainloop()


    def Reboot(self, evt = None):
        OS.system('shutdown -r 0')
        self.Quit()

    def ShutDown(self, evt = None):
        OS.system('shutdown -h 0')
        self.Quit()

    def Quit(self, evt = None):
        self.quit()     # stops mainloop
        try:
            self.destroy()  # this is necessary on Windows to prevent
                            #   Fatal Python Error: PyEval_RestoreThread: NULL tstate
        except TK.TclError:
            pass



#______________________________________________________________________
### Done! ###
#______________________________________________________________________

if __name__ == "__main__":
    ShutdownMenu()