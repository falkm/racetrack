

# Universiteit Antwerpen
# Functional Morphology
# Falk Mielke
# 2019/10/17




################################################################################
### Libraries                                                                ###
################################################################################
import os as OS
import re as RE
import sys as SYS
import time as TI                       # time, for process pause and date
import atexit as EXIT                   # commands to shut down processes
import threading as TH                  # threading for trigger
from collections import deque as DEQue  # double ended queue

import numpy as NP                      # numerics
import pandas as PD                     # data storage
import matplotlib as MP                 # plotting
import matplotlib.pyplot as PLT         # plot control
import tkinter as TK                    # GUI operations
import tkinter.messagebox as TKM        # GUI message boxes
import tkinter.filedialog as TKF        # GUI file operations

try:
    import uldaq as UL                      # MCC DAQ negotiation
    has_ul = True
except ModuleNotFoundError as err:
    print ('uldaq (MCC DAQ) not available!')
    has_ul = False
except AttributeError as err:
    print ('uldaq (MCC DAQ) not available!', err)
    has_ul = False
try:
    import wiringpi as WP            # odroid GPIO python bindings
    has_wp = True
except ModuleNotFoundError as err:
    print ('wiringpi not available!')
    has_wp = False

# connecting matplotlib and tkinter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
MP.use("TkAgg")


# disable key shortcuts of matplotlib
PLT.rcParams['keymap.save'].remove('s')
PLT.rcParams['keymap.quit'].remove('q')

################################################################################
### MCC USB1608G DAQ                                                         ###
################################################################################

instrument_labels = { '01DF5B18': 'blue' \
                    , '01DF5AFB': 'green'
                    }
if has_ul:
	status_dict = { \
                UL.ScanStatus.IDLE: 'idle' \
                , UL.ScanStatus.RUNNING: 'running' \
              }
else: 
	status_dict = {}

class MCCDAQ(object):
    # generic device functions

    # for output pin values
    LOW = 0
    HIGH = 255    

    # analog input recording mode
    recording_mode = UL.ScanOption.BLOCKIO if has_ul else None
    # recording_mode = UL.ScanOption.CONTINUOUS

    alive = False

#______________________________________________________________________
# constructor
#______________________________________________________________________

    def __init__(self, device_nr = 0):

        self.daq_device = None
        self.times = {} # storage of timing info

        # for mcc daq configuration
        self.range_index = 0

        self.AssembleAndConnect(descriptor_index = device_nr)

        self.label = str(self)

        EXIT.register(self.Quit)


    def AssembleAndConnect(self, descriptor_index = 0):
        # connect to the DAQ device
        try:
            
            interface_type = UL.InterfaceType.USB
            # Get descriptors for all of the available DAQ devices.
            devices = UL.get_daq_device_inventory(interface_type)
            number_of_devices = len(devices)
            if number_of_devices == 0:
                raise Exception('Error: No DAQ devices found')

            # print('Found', number_of_devices, 'DAQ device(s):')
            # for i in range(number_of_devices):
            #     print('  ', devices[i].product_name, ' (', devices[i].unique_id, ')', sep='')

            # Create the DAQ device object associated with the specified descriptor index.
            self.daq_device = UL.DaqDevice(devices[descriptor_index])

        ### digital input
            port_types_index = 0

            # Get the DioDevice object and verify that it is valid.
            self.digital_io = self.daq_device.get_dio_device()
            if self.digital_io is None:
                raise Exception('Error: The DAQ device does not support digital input')


            # Get the port types for the device(AUXPORT, FIRSTPORTA, ...)
            dio_info = self.digital_io.get_info()
            port_types = dio_info.get_port_types()

            if port_types_index >= len(port_types):
                port_types_index = len(port_types) - 1

            self.port = port_types[port_types_index]

        ### analog input
            # Get the AiDevice object and verify that it is valid.
            self.analog_input = self.daq_device.get_ai_device()
            if self.analog_input is None:
                raise Exception('Error: The DAQ device does not support analog input')

            # Verify that the specified device supports hardware pacing for analog input.
            self.ai_info = self.analog_input.get_info()
            if not self.ai_info.has_pacer():
                raise Exception('\nError: The specified DAQ device does not support hardware paced analog input')


        ### connect
            # Establish a connection to the DAQ device.
            # print (dir(descriptor))
            self.daq_device.connect()



        except Exception as e:
            print('constructor fail\n', e)

        print('\nConnected to', str(self), '.')


# for digital I/O
    def SetPins(self):
        # implemented by sub class
        raise TypeError('Digital pin setup not implemented!')
        pass


#______________________________________________________________________
# DAQ status
#______________________________________________________________________
    def GetDAQStatus(self):
        return self.analog_input.get_scan_status()

    def DAQIsRunning(self):
        return self.analog_input.get_scan_status()[0] is UL.ScanStatus.RUNNING

    def DAQIsIdle(self):
        return self.analog_input.get_scan_status()[0] is UL.ScanStatus.IDLE




#______________________________________________________________________
# recording preparation
#______________________________________________________________________
    def CountChannels(self, n_channels, channel_labels):
        ### channel settings
        if (n_channels is None) and (channel_labels is None):
            raise Exception('Error: please provide either channel_labels or n_channels.')

        if channel_labels is None:
            # label by number of channels per default
            self.channel_labels = [ 'a_%i' % (nr) for nr in range(n_channels) ]
        else:
            # count channels from the labels
            self.channel_labels = channel_labels
            n_channels = len(channel_labels)


        self.low_channel = 0 # record from channel...
        self.high_channel = n_channels-1 # ... to (incl) channel

        self.n_channels = n_channels



    def PrepareAnalogAcquisition(self):
        # generate settings for a recording
        # note that a single prep is usually enough for successive recordings of the same kind

        try:
            # recording_duration = 3 # s
            samples_per_channel = int(self.recording_duration*self.sampling_rate)

            # The default input mode is SINGLE_ENDED.
            input_mode = UL.AiInputMode.SINGLE_ENDED
            # If SINGLE_ENDED input mode is not supported, set to DIFFERENTIAL.
            if self.ai_info.get_num_chans_by_mode(UL.AiInputMode.SINGLE_ENDED) <= 0:
                input_mode = UL.AiInputMode.DIFFERENTIAL

            # print (dir(UL.AInScanFlag))
            flags = UL.AInScanFlag.DEFAULT

            # Get the number of channels and validate the high channel number.
            number_of_channels = self.ai_info.get_num_chans_by_mode(input_mode)
            if self.high_channel >= number_of_channels:
                self.high_channel = number_of_channels - 1
            channel_count = self.high_channel - self.low_channel + 1
            # self.StdOut (input_mode, channel_count)

            # Get a list of supported ranges and validate the range index.
            ranges = self.ai_info.get_ranges(input_mode)
            if self.range_index >= len(ranges):
                self.range_index = len(ranges) - 1


            trigger_types = self.ai_info.get_trigger_types()
            # [<TriggerType.POS_EDGE: 1>, <TriggerType.NEG_EDGE: 2>, <TriggerType.HIGH: 4>, <TriggerType.LOW: 8>]
            self.analog_input.set_trigger(trigger_types[1], 0, 0, 0, 0)


            # Allocate a buffer to receive the data.
            self.buffer = UL.create_float_buffer(channel_count, samples_per_channel)

            recording_mode = self.recording_mode

            # store settings (keywords for self.analog_input.a_in_scan)
            self.recording_settings = dict( \
                                  low_channel = self.low_channel \
                                , high_channel = self.high_channel \
                                , input_mode = input_mode \
                                , analog_range = ranges[self.range_index] \
                                , samples_per_channel = samples_per_channel \
                                , rate = self.sampling_rate \
                                , options = recording_mode \
                                , flags = flags \
                                , data = self.buffer \
                                )

        except Exception as e:
            print('\n', e)



#______________________________________________________________________
# I/O
#______________________________________________________________________
    def NOOP(self):
        # defined by subclasses
        pass

    def __str__(self):
        descriptor = self.daq_device.get_descriptor()
        return descriptor.dev_string + ' "' + instrument_labels[descriptor.unique_id] + '"'

#______________________________________________________________________
# Destructor
#______________________________________________________________________
    # def Quit(self, confirm = True):
    #     # safely exit
    #     if not self.alive:
    #         SYS.exit()
    #         return

    #     if confirm:
    #         if not TK.messagebox.askyesno("Quit","really quit?"):
    #             return

    #     if self.daq_device:
    #         # Stop the acquisition if it is still running.
    #         if not self.DAQIsIdle():
    #             self.analog_input.scan_stop()
    #         if self.daq_device.is_connected():
    #             self.daq_device.disconnect()
    #         self.daq_device.release()
    #     print('safely exited %s.' % (str(self)))
    #     self.alive = False

    #     SYS.exit()



    def __enter__(self):
        # required for context management ("with")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # exiting when in context manager
        if not self.alive:
            return

        self.Quit()


    def Quit(self):
        # safely exit
        if not self.alive:
            SYS.exit()
            return

        if self.daq_device:
            # Stop the acquisition if it is still running.
            if not self.DAQIsIdle():
                self.analog_input.scan_stop()
            if self.daq_device.is_connected():
                self.daq_device.disconnect()
            self.daq_device.release()
        print('safely exited %s.' % (self.label))
        self.alive = False

        SYS.exit()


################################################################################
### MCC DAQ single digital input                                             ###
################################################################################
class DAQ_DigitalPortInput(MCCDAQ):
    # generic device functions

#______________________________________________________________________
# Construction
#______________________________________________________________________

    def __init__(self, pin_numbers, scan_frq = 1e6, *args, **kwargs):

        # variable preparation
        self.pin_numbers = pin_numbers
        self.scan_frq = scan_frq
        super(DAQ_DigitalPortInput, self).__init__(*args, **kwargs)

        self.SetPort()

        self.alive = True


    def SetPort(self):
        # set port to input
        self.digital_io.d_config_port(self.port, UL.DigitalDirection.INPUT)
        # self.digital_io.d_config_bit(self.port, self.digital_pin, UL.DigitalDirection.INPUT)



#______________________________________________________________________
# I/O
#______________________________________________________________________

    def Read(self):
        # read out the trigger bit
        return self.digital_io.d_in(self.port)


    def Record(self):

        previous = self.Read()

        # loop until bit changes
        t0 = TI.time()
        while True:
            TI.sleep(1/self.scan_frq)

            # check trigger
            current = self.Read()

            if current != previous:
                print (TI.time()-t0, self.ByteConvert(current))

            previous = current

            if TI.time() > (t0 + 1000):
                break

    def ByteConvert(self, byte):
        return [(byte >> pin) & 1 for pin in self.pin_numbers]


#______________________________________________________________________
# Test
#______________________________________________________________________
def TestMCCPinIn(pin_nr = None):
    if pin_nr is None:
        return

    daq = DAQ_DigitalPortInput(pin_numbers = [4,5], scan_frq = 1e6)
    daq.Record()





################################################################################
### Recorder via MCC DAQ                                                     ###
################################################################################
class MCCDAQRecorder(DAQ_DigitalPortInput):
    def __init__(self, master, *args, **kwargs):
        # required arguments: master, pin_numbers 
        # optional: scan_frq, device_nr

        self.master = master
        self.queue = DEQue()
        super(MCCDAQRecorder, self).__init__(*args, **kwargs)


    def Record(self):


        # start time and values
        t0 = TI.time()
        previous = self.Read()

        # store initial state
        self.queue.append([TI.time() - t0, previous])

        # loop until bit changes
        while self.master.is_recording:
            TI.sleep(1/self.scan_frq)

            # check digital io
            current = self.Read()

            # if digital io changed:
            if current != previous:
                self.queue.append([TI.time() - t0, current])

            previous = current

            # exit condition
            if TI.time() > (t0 + 1000):
                self.queue.append([TI.time() - t0, current])
                break

        # recording ended
        self.queue.append([TI.time() - t0, current])


    def RetrieveOutput(self):
        time = []
        data_out = []
        while len(self.queue) > 0:
            t, read = self.queue.popleft()
            time.append(t)
            data_out.append( NP.array(self.ByteConvert(read)).reshape(1,-1) )

        return PD.DataFrame(NP.concatenate(data_out, axis = 0), index = time, columns = self.pin_numbers)


################################################################################
### Recorder via SoC GPIO                                                    ###
################################################################################
class GPIORecorder(object):
    mode = 0 # read
    pull = 1 # down
    
    def __init__(self, master, pin_numbers, scan_frq = 1e6):
        # required arguments: master, pin_numbers 
        # optional: scan_frq

        self.master = master
        self.queue = DEQue()
        self.pin_numbers = pin_numbers
        self.scan_frq = scan_frq


        WP.wiringPiSetup()
        self.ConfigurePins()

    def ConfigurePins(self):
        for pin in self.pin_numbers:
            WP.pinMode(pin, self.mode)
            WP.pullUpDnControl(pin, self.pull)




#______________________________________________________________________
# I/O
#______________________________________________________________________

    def Read(self):
        # read out the trigger bit
        return [WP.digitalRead(pin) for pin in self.pin_numbers]


    def Record(self):

        # start time and values
        t0 = TI.time()
        previous = self.Read()

        # store initial state
        self.queue.append([TI.time() - t0, previous])

        # loop until bit changes
        while self.master.is_recording:
            TI.sleep(1/self.scan_frq)

            # check digital io
            current = self.Read()

            # if digital io changed:
            if current != previous:
                #print (previous, current)
                self.queue.append([TI.time() - t0, current])

            previous = current

            # exit condition
            if TI.time() > (t0 + 1000):
                self.queue.append([TI.time() - t0, current])
                break

        # recording ended
        self.queue.append([TI.time() - t0, current])



    #def ByteConvert(self, byte):
    #    return [(byte >> nr) & 1 for nr, pin in enumerate(self.pin_numbers)]


    def RetrieveOutput(self):
        time = []
        data_out = []
        while len(self.queue) > 0:
            t, read = self.queue.popleft()
            time.append(t)
            data_out.append( NP.array(read).reshape(1,-1) )

        return PD.DataFrame(NP.concatenate(data_out, axis = 0), index = time, columns = self.pin_numbers)


    def __enter__(self):
        # required for context management ("with")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # exiting when in context manager
        self.Quit()


    def Quit(self):
        # safely exit
        SYS.exit()
        return



################################################################################
### RaceTrack GUI                                                            ###
################################################################################
class RaceTrack(TK.Tk):
    def __init__(self, pin_numbers, scan_frq = 1e6, beam_positions_m = None, beam_distance = None, use_gpio = False, *args, **kwargs):
        # pin_numbers: list of numbers 
        # scan_frq: sampling frequency 

        # get a GUI master
        super(RaceTrack, self).__init__(*args, **kwargs)
        self.attributes("-fullscreen", True)

        # prepare the GUI
        self.MakeFigure()
        self.MakeGUI()

        # initialize the numbers
        self.counter = 1 # counting number of recordings
        self.PrepareAutosave()

        self.data = None # data storage
        self.is_recording = False # recording in progress?
        self.saved = True # data has been stored by the user
        self.recorder_thread = None # thread for recording in the background

        self.pin_numbers = pin_numbers # pins to store
        if use_gpio:
            self.recorder = GPIORecorder(master = self, pin_numbers = self.pin_numbers, scan_frq = scan_frq)
        else:
            self.recorder = MCCDAQRecorder(master = self, pin_numbers = self.pin_numbers, scan_frq = scan_frq)

        self.beam_positions = beam_positions_m # for speed estimation
        self.beam_distance = beam_distance

        # begin the GUI main process
        self.UpdatePlot()
        self.mainloop()

#______________________________________________________________________
### data manipulation
    def OrganizeNewData(self, new_data):
        new_data.index.name = 'time'
        new_data.loc[0] = new_data.iloc[0, :].values
        self.data = new_data
        self.data.sort_index(inplace = True)
        self.saved = False
        # print (self.data)



#______________________________________________________________________
### keyboard shortcuts
    def PressKey(self, event):
        # print('pressed', event.key, event.xdata, event.ydata)
        if event.key is None:
            return
        # if event.key == '???':
        #     self.prefix = 'x'
        #     return
        
        # event.key = event.key.replace('ctrl+','')
        self.keyboard_shortcuts.get(event.key, self.NOOP )()

    def ReleaseKey(self, event):
        pass
        # print('released', event.key, event.xdata, event.ydata)
        # if event.key == 'control':
        #     self.prefix = ''
        

#______________________________________________________________________
### callbacks
    def NOOP(self):
        pass

    def ToggleRecording(self):
        if self.is_recording:
            self.is_recording = False
            self.recorder_thread.join()
            self.recorder_thread = None

            self.StdOut('done recording!')
            self.hold_my_beer.set_visible(False)
            self.OrganizeNewData(self.recorder.RetrieveOutput())
            self.UpdatePlot()
            return 

        if not self.saved:
            if not TKM.askyesno("Unsaved data!", "Previous recording not saved, will be overwritten. Really start new recording?"):
                return

        self.is_recording = True
        self.StdOut('recording...')
        self.hold_my_beer.set_visible(True)
        self._canvas.draw()

        self.recorder_thread = TH.Thread(target = self.recorder.Record)
        self.recorder_thread.daemon = True
        self.recorder_thread.start()



    def Save(self):
        if self.data is None:
            self.StdOut("nothing to save.")
            return

        outfile = TKF.asksaveasfilename( \
                                  defaultextension = ".csv" \
                                , filetypes = [('comma separated values', '.csv'), ('all files', '.*')] \
                                , title = 'Select a file to save the recording' \
                                , initialdir = 'recordings' \
                                , initialfile = "%s_rec%i_racetrack.csv" % (TI.strftime('%Y%m%d'), self.counter) \
                                )
        if len(outfile) == 0:
            self.StdOut('not saved!')
            return
    
    
        # print (self.labels.columns)
        self.data.to_csv( \
                              outfile \
                            , sep = ';' \
                            , header = True \
                            , float_format = '%.6f' \
                            , index = True \
                            )
        self.StdOut("saved! %s" % (outfile))
        self.saved = True
        self.counter += 1



    def PrepareAutosave(self):
        previous_recordings = [file for file in OS.listdir('recordings') if OS.path.splitext(file)[1] == '.csv']
        counts = [0]
        for file in previous_recordings:
            filename = OS.path.splitext(file)[0]
            found = RE.findall(r"(?<=_rec)\d*_", filename) # find file name patterns of the type "*daqXXX_*"
            # print (filename, found)
            if not (len(found) == 0):
                counts.append(int(found[0][:-1]))

        self.counter = max(counts) + 1



    def EstimateSpeed(self):
        # assuming all beams are closed per default;
        # taking the first interrupt to calculate speed

        # return if no data is recorded
        if self.data is None:
            return

        # adjust beam positions
        if self.beam_positions is None:
            if self.beam_distance is not None:
                self.beam_positions = NP.cumsum( [self.beam_distance] * len(self.pin_numbers) )
            else:
                self.beam_positions = NP.cumsum([1] * ( len(self.pin_numbers) ))

        # get covered distances 
        beam_distances = NP.diff(NP.array(self.beam_positions))

        # loop pin triggers
        times = []
        distances = []
        previous_trigger = NP.argmin(self.data[self.pin_numbers[0]].values)
        skipper = 1
        for nr, pin in enumerate(self.pin_numbers[1:]):
            if NP.all(self.data[pin].values) or NP.all(NP.logical_not(self.data[pin].values)):
                skipper += 1
                continue
            
            trigger = NP.argmin(self.data[pin].values)
            trigger_time = self.data.index[trigger]

            times.append(trigger_time - self.data.index[previous_trigger])
            distances.append(beam_distances[nr] * skipper)

            previous_trigger = trigger
            skipper = 1

        speeds = NP.divide(NP.array(distances), NP.array(times))

        self.StdOut(f"""average {NP.sum(distances)/NP.sum(times):.4f} m/s, max {NP.max(NP.abs(speeds)):.4f} m/s, intervals [{";".join(map(lambda txt: "%.1e" % (txt), speeds))}]""" )


    def StdOut(self, text):
        self.indicator.set(text)


#______________________________________________________________________
### gui creation
    def MakeGUI(self):

        # http://matplotlib.org/users/event_handling.html
        self.keyboard_shortcuts = { }
        self.keyboard_shortcuts[' '] = self.ToggleRecording
        self.keyboard_shortcuts['s'] = self.Save
        self.keyboard_shortcuts['q'] = self.Quit
        self.keyboard_shortcuts['v'] = self.EstimateSpeed
        self.keyboard_shortcuts['escape'] = self.Quit


        buttons_frame = TK.Frame(self)
        buttons_frame.pack(side = TK.TOP, fill=TK.X, expand=False)
        # buttons_class = []
        # for ct in sorted(usv_category_shortcuts.keys()):
        #     buttons_class.append(TK.Button(master = buttons_frame \
        #                                     , text = "%s (%s)" % (usv_category_shortcuts[ct], ct) \
        #                                    , command = lambda x = ct: self.ButtonClassification(x))\
        #                         )
        #     buttons_class[-1].pack(side = TK.LEFT)

        button_quit = TK.Button(master = buttons_frame, text='quit [q]', command=self.Quit \
                                , height = 4, padx = 20 \
                                )
        button_quit.pack(side = TK.RIGHT) 

        button_estimate = TK.Button(master = buttons_frame, text='estimate speed [v]', command=self.EstimateSpeed \
                                , height = 4, padx = 20 \
                                )
        button_estimate.pack(side = TK.RIGHT) 

        button_save = TK.Button(master = buttons_frame, text='save [s]', command=self.Save \
                                , height = 4, padx = 20 \
                                )
        button_save.pack(side = TK.RIGHT) 

        button_record = TK.Button(master = buttons_frame, text='record [space]', command=self.ToggleRecording \
                                , height = 4, padx = 20 \
                                )
        button_record.pack(side = TK.RIGHT) 

        self.indicator = TK.StringVar()
        TK.Label(master = buttons_frame, textvariable = self.indicator).pack(side = TK.RIGHT)

        self.indicator.set("")


        self._canvas = FigureCanvasTkAgg(self.fig, master = self)
        self._canvas.get_tk_widget().pack(side = TK.TOP, fill = TK.BOTH, expand = 1) #.grid(row=0, column=1, rowspan=3) #
        self._canvas.draw()
        self._canvas.mpl_connect('key_press_event', self.PressKey)
        self._canvas.mpl_connect('key_release_event', self.ReleaseKey)

        toolbar = NavigationToolbar2Tk( self.fig.canvas, self )
        toolbar.pack(side = TK.TOP, fill=TK.X, expand=0) #.grid(row=3, column=1) #
        toolbar.update()

        


#______________________________________________________________________
### plotting
    def MakeFigure(self):
    # set figure size
        # to get to centimeters, the value is converted to inch (/2.54) 
        #                        and multiplied with empirical factor (*1.25).
        figwidth = 12 #cm
        figheight = 8 #cm

    # define figure
        self.fig = PLT.figure( \
                                  figsize = (figwidth/2.54*1.25, figheight/2.54*1.25) \
                                , facecolor = None \
                                , dpi = 150 \
                                )
        # self.fig.hold(True) #(if multiple figures)

        # PLT.ion() # "interactive mode". Might be useful here, but i don't know. Try to turn it off later.

    # define axis spacing
        self.fig.subplots_adjust( \
                                  top = 0.99 \
                                , right = 0.99 \
                                , bottom = 0.10 \
                                , left = 0.10 \
                                , wspace = 0.2 \
                                , hspace = 0.3 \
                                )

    # # a supertitle for the figure; relevant if multiple subplots
    #     self.fig.suptitle( r"not my $%i^{st}$ plot" % ( 1 ) )

    # get an axis
        self.ax = self.fig.add_subplot(111)


    # axis cosmetics
        self.ax.get_xaxis().set_tick_params(which='both', direction='out')
        self.ax.get_yaxis().set_tick_params(which='both', direction='out')
        self.ax.tick_params(top=False)
        self.ax.tick_params(right=False)
        self.ax.tick_params(left=False)
        self.ax.tick_params(right=False)

        self.ax.spines['top'].set_visible(False)
        self.ax.spines['bottom'].set_visible(True)
        self.ax.spines['left'].set_visible(False)
        self.ax.spines['right'].set_visible(False)


    def EmptyPlot(self):
        self.ax.cla()
        self.hold_my_beer = self.ax.annotate( \
                      "Recording in progress!" \
                    , xy = (0.5, 0.5) \
                    , xycoords = "figure fraction" \
                    , xytext = (0.5, 0.5) \
                    , textcoords = "figure fraction" \
                    , ha = 'center', va = 'center' \
                    , color = (0.8, 0.3, 0.3) \
                    )
        self.hold_my_beer.set_visible(self.is_recording)


    def UpdatePlot(self):

        self.EmptyPlot()
        if self.data is None:
            self._canvas.draw()
            return

        for pin in self.pin_numbers:
            self.ax.step( self.data.index \
                        , self.data[pin].values \
                        , where = 'post' \
                        , label = 'beam %i' % (pin) \
                        )

        # self.StdOut()
        self.ax.set_ylim(-0.01, 1.01)
        self.ax.legend(loc = 4)
        self._canvas.draw()
        # print (self.data)


#______________________________________________________________________
### exit
    def Quit(self):
        if TKM.askyesno("please don't go...",'really quit?%s' % ('' if self.saved else '\n UNSAVED DATA!')):
            PLT.close(self.fig)
            SYS.exit()

  

"""
#######################################################################
### Mission Control                                                 ###
#######################################################################
"""
if __name__ == "__main__":

    # TestMCCPinIn(4)
    gui = RaceTrack(pin_numbers = [0,1,2,3,4,5,6,7], scan_frq = 1e9, beam_distance = 0.56/8., use_gpio = has_wp )

